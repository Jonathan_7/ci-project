#!/usr/bin/env bash

mysql < tests/assets/teardown.sql
mysql < tests/assets/create.sql
mysql < tests/assets/data.sql

vendor/bin/phpunit tests
